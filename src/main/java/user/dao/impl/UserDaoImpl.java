package user.dao.impl;

import org.springframework.stereotype.Repository;
import user.beans.BaseBean;
import user.dao.BaseDao;
import user.dao.UserDao;
import user.database.entities.BaseEntity;
import user.database.entities.user.UserCustomerEntity;
import user.database.entities.user.UserEntity;

import java.util.List;

/**
 * Created by atticus on 18.06.17.
 */
@Repository
public class UserDaoImpl extends BaseDao implements UserDao {
    private static final int CACHE_MAX_SIZE = 200;


    public UserEntity getUsersEntityByUserNameAndPassword(String user, String password) {

        String str_query = "from UserEntity where login='" + user + "' AND password='" + password + "'";
        List<UserEntity> lst_systemEntities = (List<UserEntity>) super.executeQuery(str_query);
        if(!lst_systemEntities.isEmpty()) {
            UserEntity currencyEntity =  lst_systemEntities.get(0);
            return currencyEntity;
        }
        else return  null;

    }

    public void saveOrUpdateUserEntity(BaseEntity baseEntity) {
        super.save(baseEntity);
    }


    public boolean checkOnDuplicateLogin(String login) {
        String str_query = "from UserEntity where login='" + login+"'";
        List<UserEntity> lst_systemEntities = (List<UserEntity>) super.executeQuery(str_query);
        if(!lst_systemEntities.isEmpty()) {
            return false;
        }
        else return  true;

    }
}
