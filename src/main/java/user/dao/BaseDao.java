package user.dao;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.springframework.stereotype.Component;
import user.database.entities.BaseEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Component
public class BaseDao {



    @PersistenceContext
    private EntityManager entityManager;

    public EntityManager getEntityManager() {
        return entityManager;
    }


    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(BaseEntity baseEntity) {
        entityManager.persist(baseEntity);
    }

    public void detached(BaseEntity baseEntity) {
        entityManager.detach(baseEntity);
    }

    public List<BaseEntity> save(List<BaseEntity> a_lstBaseEntity) {

        //entityManager.getTransaction().begin();

        for (BaseEntity baseEntity : a_lstBaseEntity) {
            entityManager.persist(baseEntity);
        }
        //entityManager.flush();
        //entityManager.clear();

        //entityManager.getTransaction().commit();
        return a_lstBaseEntity;
    }

    public void update(BaseEntity baseEntity) {
        entityManager.merge(baseEntity);
    }

    public List<BaseEntity> update(List<BaseEntity> a_lstBaseEntity) {

        entityManager.getTransaction().begin();

        for (BaseEntity baseEntity : a_lstBaseEntity) {
            entityManager.merge(baseEntity);
        }
        entityManager.flush();
        entityManager.clear();

        entityManager.getTransaction().commit();
        return a_lstBaseEntity;
    }

    public List executeQuery(String a_strQuery) {

        Query query = entityManager.createQuery(a_strQuery);
        List list_results = query.getResultList();
        return list_results;
    }

    public List executeNativeQuery(String a_strQuery) {

        Query query = entityManager.createNativeQuery(a_strQuery);
        List list_results = query.getResultList();
        return list_results;
    }

    public List executeNativeQueryPaging(String a_strQuery, Integer offset, Integer maxResults) {

        Query query = entityManager.createNativeQuery(a_strQuery);
        query.setMaxResults(maxResults);
        query.setFirstResult(offset);
        List list_results = query.getResultList();
        return list_results;
    }

    public List executeQueryPaging(String a_strQuery, Integer offset, Integer maxResults) {

        Query query = entityManager.createQuery(a_strQuery);
        query.setMaxResults(maxResults);
        query.setFirstResult(offset);
        List list_results = query.getResultList();
        return list_results;
    }

    /*  public List getEntitiesByFields(Class baseEntity, String[] filedName, String[] fieldValue) {

          String str_className = baseEntity.getLogin();
          String str_query = "from " + str_className + " where " + filedName[0] + "=:" + filedName[0];
          if (filedName.length > 1) {
              for (int i = 1; i < filedName.length; i++) {
                  str_query = str_query + " and " + filedName[i] + "=:" + filedName[i];
              }
          }

          Query query = entityManager.createQuery(str_query);

          for (int i = 0; i < filedName.length; i++) {

              query.setParameter(filedName[i], fieldValue[i]);
          }
          List list_results = query.getResultList();
          return list_results;
      }*/
    public List getEntitiesByFields(Class baseEntity, String parentField, String parentValue, String roleField, String roleValue) {
        String str_className = baseEntity.getName();

        String str_query = "from " + str_className + " where " + parentField + "=" + parentValue + " and " + roleField + "='" + roleValue + "'";
        System.out.println("Query is : " + str_query);
        Query query = entityManager.createQuery(str_query);
        // query.setParameter(parentField, Long.parseLong(parentValue));
        // query.setParameter(roleField, roleValue);

        List list_results = query.getResultList();

        return list_results;
    }

    public List getEntitiesByFieldString(Class baseEntity, String parentField, String parentValue, String roleField, String roleValue) {
        String str_className = baseEntity.getName();

        String str_query = "from " + str_className + " where " + parentField + "= '" + parentValue + "' and " + roleField + "='" + roleValue + "'";
        System.out.println("Query is : " + str_query);
        Query query = entityManager.createQuery(str_query);
        // query.setParameter(parentField, Long.parseLong(parentValue));
        // query.setParameter(roleField, roleValue);

        List list_results = query.getResultList();

        return list_results;
    }

 /*   public void save(BaseEntity[] lst_baseEntity) {
        for(int i = 0; i < lst_baseEntity.length; i++){
            this.sessionFactory.getCurrentSession().saveOrUpdate(lst_baseEntity[i]);
        }
    }

    public void update(BaseEntity[] lst_baseEntity) {
        for(int i = 0; i < lst_baseEntity.length; i++){
            this.sessionFactory.getCurrentSession().update(lst_baseEntity[i]);
        }
    }

    public void update(BaseEntity baseEntity) {
        this.sessionFactory.getCurrentSession().update(baseEntity);
    }

    public List getAllEntities(Class baseEntity) {
        String query = "from "+baseEntity.getLogin();
        return this.sessionFactory.getCurrentSession().createQuery(query).list();
    }

    public List getAllEntities(Class baseEntity, String orderByProperty) {
        String query = "from "+baseEntity.getLogin()+" objectAlis ORDER BY objectAlis."+orderByProperty +" desc";
        return this.sessionFactory.getCurrentSession().createQuery(query).list();
    }

    public List getAllEntitiesAsc(Class baseEntity, String orderByProperty) {
        String query = "from "+baseEntity.getLogin()+" objectAlis ORDER BY objectAlis."+orderByProperty;
        return this.sessionFactory.getCurrentSession().createQuery(query).list();
    }

    public List getAllEntityByQuery(String query) {
        return this.sessionFactory.getCurrentSession().createQuery(query).list();
    }


    public List getEntityByField(BaseEntity baseEntity,String filedName,String fieldValue) {

        String str_className = baseEntity.getClass().getLogin();
        String str_query = "from "+str_className +" where "+filedName+"=:"+filedName;
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        query.setParameter(filedName, fieldValue);
        List list = query.list();
        return list;
    }

    public List getEntityByField(Class baseEntity,String filedName,String fieldValue) {

        String str_className = baseEntity.getLogin();
        String str_query = "from "+str_className +" where "+filedName+"=:"+filedName;
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        query.setParameter(filedName, fieldValue);
        List list = query.list();
        return list;
    }

    public List getEntityByField(Class baseEntity,String filedName,String fieldValue,String strOrderBy) {

        String str_className = baseEntity.getLogin();
        String str_query = "from "+str_className +" where "+filedName+"=:"+filedName +" ORDER BY "+strOrderBy+" desc" ;
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        query.setParameter(filedName, fieldValue);
        List list = query.list();
        return list;
    }

   List getEntityByFields(BaseEntity baseEntity,String[] filedName,String[] fieldValue) {

        String str_className = baseEntity.getClass().getLogin();
        String str_query = "from "+str_className +" where "+filedName[0]+"=:"+filedName[0];
        if(filedName.length > 1){
            for(int i = 1; i < filedName.length; i++){
                str_query = str_query + " and "+filedName[i]+"=:"+filedName[i];
            }
        }
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);

        for(int i = 0; i < filedName.length; i++){
            query.setParameter(filedName[i], fieldValue[i]);
        }
        List list = query.list();
        return list;
    }

    public List getEntityByFields(Class baseEntity,String[] filedName,String[] fieldValue) {

        String str_className = baseEntity.getClass().getLogin();
        String str_query = "from "+str_className +" where "+filedName[0]+"=:"+filedName[0];
        if(filedName.length > 1){
            for(int i = 1; i < filedName.length; i++){
                str_query = str_query + " and "+filedName[i]+"=:"+filedName[i];
            }
        }
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);

        for(int i = 0; i < filedName.length; i++){
            query.setParameter(filedName[i], fieldValue[i]);
        }
        List list = query.list();
        return list;
    }

    public List getEntityByFieldsOrOperator(BaseEntity baseEntity,String[] filedName,String[] fieldValue) {

        String str_className = baseEntity.getClass().getLogin();
        String str_query = "from "+str_className +" where "+filedName[0]+"=:"+filedName[0];
        if(filedName.length > 1){
            for(int i = 1; i < filedName.length; i++){
                str_query = str_query + " or "+filedName[i]+"=:"+filedName[i];
            }
        }
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        for(int i = 0; i < filedName.length; i++){
            query.setParameter(filedName[i], fieldValue[i]);
        }
        List list = query.list();
        return list;
    }


    public List getEntityByDates(Class baseEntity,String fieldName,Date startDate,Date endDate) {

        String str_className = baseEntity.getLogin();
        String str_query = "from "+str_className +" where "+fieldName+">=:startDate and "+fieldName+" <=:endDate";
        //String str_query = "from "+str_className + where  :startDate and :endDate";
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        query.setParameter("startDate", startDate);
        query.setParameter("endDate", endDate);
        List list = query.list();
        return list;
    }

    public List getEntityById(Class baseEntity,Long id) {

        String str_className = baseEntity.getLogin();
        String str_query = "from "+str_className +" where id ="+id;
        Query query = this.sessionFactory.getCurrentSession().createQuery(str_query);
        List list = query.list();
        return list;
    }


    public Session getSession(){
        return this.sessionFactory.getCurrentSession();
    }

    private String getTableName(BaseEntity baseEntity){
        String str_tableName = null;
        Class aClass = baseEntity.getClass();
         Annotation[] annotations = aClass.getAnnotations();
         for (Annotation annotation : annotations) {
             if (annotation instanceof Table) {
                 Table myAnnotation = (Table) annotation;
                 str_tableName = myAnnotation.name();
             }
         }
         return str_tableName;
    }
    */

    public Long countEntityItems(Class aClass) {

        String query = "Select count(*) from " + aClass.getName();


        Object singleResult = entityManager.createQuery(query).getSingleResult();

        System.out.println("Single REsult is : " + singleResult.toString());

        return Long.parseLong(singleResult.toString());

    }

    public List getAllEntities(Class baseEntity) {
        String query = "from " + baseEntity.getName() + " order by id asc";
        Query aquery = entityManager.createQuery(query);
//        aquery.setMaxResults(10);
        aquery.setFirstResult(0);
        List list_results = aquery.getResultList();
        return  list_results;
//        return entityManager.createQuery(query).getResultList();
    }

    public List getAllEntitiesNameAsc(Class baseEntity) {
        String query = "from " + baseEntity.getName() + " order by name asc";
        return entityManager.createQuery(query).getResultList();
    }

    public Object getEntityById(Class baseEntity, Long id) {

        String str_className = baseEntity.getName();
        String str_query = "from " + str_className + " where id =" + id;
        // entityManager.createQuery(str_query).getSingleResult();

        return entityManager.createQuery(str_query).getSingleResult();
    }

    public List<Object> getEntitiesById(Class baseEntity, Long id) {

        String str_className = baseEntity.getName();
        String str_query = "from " + str_className + " where id =" + id;
        // entityManager.createQuery(str_query).getSingleResult();

        return entityManager.createQuery(str_query).getResultList();
    }

    public List<Object> getEntitiesByUserId(Class baseEntity, Long id) {

        String str_className = baseEntity.getName();
        String str_query = "from " + str_className + " where userId =" + id;
        // entityManager.createQuery(str_query).getSingleResult();

        return entityManager.createQuery(str_query).getResultList();
    }

    public Object getCustomerEntityById(Class baseEntity, Long id) {

        String str_className = baseEntity.getName();
        String str_query = "from " + str_className + " where userId =" + id;
        // entityManager.createQuery(str_query).getSingleResult();

        return entityManager.createQuery(str_query).getSingleResult();
    }

    public List getEntityByField(Class baseEntity, String filedName, String fieldValue) {

        String str_className = baseEntity.getName();
        String str_query = "from " + str_className + " where " + filedName + "=:" + filedName + " order by id desc";

        return entityManager.createQuery(str_query).setParameter(filedName, fieldValue).getResultList();
    }

    public List getAllEntitiesAsc(Class baseEntity, String orderByProperty) {
        String query = "from " + baseEntity.getName() + " objectAlis ORDER BY objectAlis." + orderByProperty;
        return entityManager.createQuery(query).getResultList();
    }

    public List getAllEntities(Class baseEntity, String orderByProperty) {
        String query = "from " + baseEntity.getName() + " objectAlis ORDER BY objectAlis." + orderByProperty + " desc";
        return entityManager.createQuery(query).getResultList();
    }

    public List getResultAgainstQuery(String str_query) {

        List lst_systemUser = entityManager.createQuery(str_query).getResultList();

        return lst_systemUser;
    }

    public List getResultAgainstQueryMaxResults(String str_query, Integer maxResults) {

        List lst_systemUser = entityManager.createQuery(str_query).setMaxResults(maxResults).getResultList();

        return lst_systemUser;
    }

    public List getResultAgainstQuery(String str_query, Class a_class) {
        List lst_systemUser = entityManager.createNativeQuery(str_query, a_class).getResultList();
        return lst_systemUser;
    }


    public String getTodayTransactionCountAgainstUser(String userId) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String str_query = "SELECT COUNT(TransactionsSender.CreatedAt) FROM TransactionsSender INNER Join Transactions ON TransactionsSender.TransId = Transactions.Id where TransactionsSender.UserId = " + userId + " AND Transactions.Action = 'loadCard' AND DATE_FORMAT(Transactions.CreatedAt, '%Y-%m-%d') = DATE_FORMAT(CURRENT_DATE, '%Y-%m-%d')";
        // String str = "Select count(msg) from TransactionSenderEntity msg where day(msg.createdAt) = :date and month(msg.createdAt) = :month and year(msg.createdAt) = :year and msg.userId="+userId;
        Query query = entityManager.createNativeQuery(str_query);
        Object result = query.getSingleResult();
        if (result != null) {
            return result.toString();
        } else {
            System.out.println("Result is null");
            return "0";
        }

    }

    public String getTotalTransactionAmountAgainstUser(String userId) {

        String str = "SELECT " +
                "Sum(Transactions.TotalAmount) " +
                "FROM " +
                "TransactionsSender " +
                "Inner Join Transactions ON TransactionsSender.TransId = Transactions.Id " +
                "WHERE " +
                "TransactionsSender.UserId =  " + userId + " AND Transactions.Action = 'loadCard' AND " +
                "Transactions.CreatedAt >=  '2016-01-04 17:04:57' AND " +
                "Transactions.CreatedAt <=  '2016-02-04 17:04:57'";
        Query query = entityManager.createNativeQuery(str);
        Object result = query.getSingleResult();
        if (result != null) {
            return result.toString();
        } else {
            System.out.println("Result is null");
            return "0";
        }

    }
}
