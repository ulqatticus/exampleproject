package user.dao;

import user.database.entities.BaseEntity;
import user.database.entities.user.UserCustomerEntity;
import user.database.entities.user.UserEntity;

/**
 * Created by atticus on 18.06.17.
 */
public interface UserDao  {
    UserEntity getUsersEntityByUserNameAndPassword(String user, String password);


    public void saveOrUpdateUserEntity(BaseEntity baseEntity);
    boolean checkOnDuplicateLogin(String login);
}
