package user.controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import user.beans.CustomerBean;
import user.beans.UserBean;
import user.database.entities.user.UserCustomerEntity;
import user.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by atticus on 08.06.17.
 */
@Controller
public class controller {

    @Autowired
    UserFacade userFacade;


    @GetMapping("/login")
    public ModelAndView displayLogin(ModelMap map, @ModelAttribute("UserBean") UserBean userbean,
                                     @RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }
        model.setViewName("login");
        return model;
    }

    @PostMapping(value = "/join")
    public ModelAndView join(ModelMap map, HttpSession session, @ModelAttribute("UserBean") UserBean userbean, @RequestParam(value = "error", required = false) String error) {
        ModelAndView model;

        UserBean uBean = userFacade.checkUser(userbean.getLogin(), userbean.getPassword());
        if (uBean != null) {
            model = new ModelAndView();
            model.setViewName("/welcome_page");
            session.setAttribute("userId", userbean.getId());

        } else {
            model = new ModelAndView();
            model.addObject("error", "Invalid user name or password");
            model.setViewName("redirect:/login");
        }
        return model;
    }

    @GetMapping("/registration")
    public ModelAndView registration(@ModelAttribute("CustomerBean") CustomerBean customerBean, @ModelAttribute("UserBean") UserBean userBean) {
        //    StringUtil.printMessageOnConsole("Online Module Login Screen");
        ModelAndView model = new ModelAndView();
        model.setViewName("reg");
        return model;
    }

    @PostMapping(value = "/registrationNew")
    public String registrationNewUser(@ModelAttribute("CustomerBean") CustomerBean customerBean, @ModelAttribute("UserBean") UserBean userBean,
                                      BindingResult bindingResult, RedirectAttributes attributes) {
        //    StringUtil.printMessageOnConsole("Online Module Login Screen");
        if (!bindingResult.hasErrors()) {
            if(userFacade.isLoginFree(userBean)) {
                userFacade.newUser(customerBean, userBean);
                attributes.addFlashAttribute("success", "You have been successfully registered");
            }
            else{
                attributes.addFlashAttribute("loginErr", "login already exist");
            }
        }
        return "redirect:/registration";
    }
}


