package user.utils;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class DateFormatManager {

    private static String DATE_FORMAT_DD_MM_YY = "dd-mm-yy";
    public static String DATE_FORMAT_FORM = "dd-MM-yyyy";
    private static String DATE_FORMAT_DD_MM_YY_HH_MM_SS = "dd-MM-yy HH:mm:ss";
    public static String DATE_FORMAT_TO_DISPLAY = "dd-MM-yy HH:mm:ss";
    public static String DATE_FORMAT_DATA_BASE = "yyyy-MM-dd HH:mm:ss";
    public static String DATE_FORMAT_DATA_BASE_DATE = "yyyy-MM-dd";
    public static String DATE_FORMAT_DATA_BASE_TIME_ZONE = "yyyy-MM-dd HH:mm:ss'Z'";
    private static String DATE_FORMAT_MT103 = "yyMMdd";
    private static String DATE_FORMAT_UK_3_DAYS = "ddMMyy";
    private static String DATE_FORMAT_WITH_POINT = "dd.MMM.yyyy";



    public static Date getUTCDateTime() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcTime = sdf.format(new Date());

        SimpleDateFormat sdf_utc = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        Date date = sdf_utc.parse(utcTime);

        return date;
    }

    public static String getUTCDateTimeString() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static Date getUTCDateDate() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcTime = sdf.format(new Date());

        SimpleDateFormat sdf_utc = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        Date date = sdf_utc.parse(utcTime);
        return date;
    }

    public static String getUTCDateString() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static String getUTCDateStringPlusDays(int days) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, days);
        String utcTime = sdf.format(c.getTime());
        return utcTime;
    }

    public static String parseDateDDMMYYFormat(String a_strDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        Date utcTime = sdf.parse(a_strDateInForm);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        sdf.format(date);
        String str_utcTime = sdf.format(date);

        return str_utcTime;
    }





    /**
     *
     * @param a_strDateInForm
     * @return
     * @throws Exception
     */
    public static Date parseFormDateToDataBaseDate(String a_strDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        Date utcTime = sdf.parse(a_strDateInForm);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.format(date);
        String str_utcTime = sdf.format(date);

        SimpleDateFormat sdf_utc = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        Date utcDate = sdf_utc.parse(str_utcTime);

        return utcDate;
    }

    /**
     *
     * @param a_strDateInForm
     * @return
     * @throws Exception
     */
    public static String parseDefaultDateFormatToDataBaseDate(String a_strDateInForm) throws Exception{

        DateFormat originalFormat = new SimpleDateFormat("dd-mm-yyyy");
        DateFormat targetFormat = new SimpleDateFormat("yyyy-mm-dd HH:MM:ss");
        Date date = originalFormat.parse(a_strDateInForm);
        String formattedDate = targetFormat.format(date);

        return formattedDate;
    }

    /**
     *
     * @param a_strDateInForm
     * @return
     * @throws Exception
     */
    public static String parseDataBaseDateToFormDate(String a_strDateInForm) throws Exception{

        //SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        Date utcTime = sdf.parse(a_strDateInForm);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.format(date);
        String str_utcTime = sdf.format(date);

        return str_utcTime;
    }

    public static String parseDataBaseDateToFormDate(Date a_dtDateInForm) throws Exception{

        SimpleDateFormat sdfDb = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        String str_dbDate = sdfDb.format(a_dtDateInForm);


        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE);
        Date utcTime = sdf.parse(str_dbDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        //sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        sdf.format(date);
        String str_utcTime = sdf.format(date);

        return str_utcTime;
    }


    public static Date parseStringDateToFormDate(String a_strDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        Date dateForm = sdf.parse(a_strDateInForm);
        return dateForm;
    }

    public static String parseFormDateToStringDate(Date a_dtDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_FORM);
        String str_utcTime = sdf.format(a_dtDateInForm);
        return str_utcTime;
    }

    /**
     *
     * @param a_strDateInForm
     * @return
     * @throws Exception
     */
    public static String parseDateToMt103Format(String a_strDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        Date utcTime = sdf.parse(a_strDateInForm);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_MT103);
        sdf.format(date);
        String str_utcTime = sdf.format(date);

        return str_utcTime;
    }

    /**
     *
     * @param a_strDateInForm
     * @return
     * @throws Exception
     */
    public static String parseDateToMt103UK3DaysFormat(String a_strDateInForm) throws Exception{

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY_HH_MM_SS);
        Date utcTime = sdf.parse(a_strDateInForm);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(utcTime);
        Date date = calendar.getTime();

        sdf = new SimpleDateFormat(DATE_FORMAT_UK_3_DAYS);
        sdf.format(date);
        String str_utcTime = sdf.format(date);
        return str_utcTime;
    }

    /**
     *
     * @return
     */
    public static Date getCurrentDate() {
        String str_date = null;
        Date    obj_date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatManager.DATE_FORMAT_DD_MM_YY);
            Date date = new Date();
            str_date = dateFormat.format(date);
            obj_date = dateFormat.parse(str_date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj_date;
    }

    public static Date getCurrentDateFormFormat() {
        String str_date = null;
        Date    obj_date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DateFormatManager.DATE_FORMAT_FORM);
            Date date = new Date();
            str_date = dateFormat.format(date);
            obj_date = dateFormat.parse(str_date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return obj_date;
    }

    public static List<Date> getStartAndEndDateToday() {

        List<Date> lst_dates = new ArrayList<Date>();
        Calendar calendar = Calendar.getInstance();

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH); // Note: zero based!
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        Calendar calendarStartDateCal = new GregorianCalendar(year, month, day);
        Date startDate = calendarStartDateCal.getTime();
        lst_dates.add(startDate);

        Calendar calendarEndDateCal = new GregorianCalendar(year, month, day,23,59);
        Date endDate = calendarEndDateCal.getTime();
        lst_dates.add(endDate);

        return lst_dates;
    }

    public static Date getStringToDateSimpleFormat(String a_date) {
        Date date = null;
        try {
            //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatManager.DATE_FORMAT_DD_MM_YY);
            date = simpleDateFormat.parse(a_date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    public static String getStringToDateTimeDateFormFormat(String a_datetimeForForm) {
        Date date = null;
        String utcTime_string = null;
        try {
            String DATE_FORMAT_DATA_BASE_DATE_TIME_DEFAULT = "dd-MM-yyyy HH:mm:ss";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE_TIME_DEFAULT);
            date = simpleDateFormat.parse(a_datetimeForForm);


            SimpleDateFormat sdf_string = new SimpleDateFormat(DATE_FORMAT_FORM);
            utcTime_string = sdf_string.format(date);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return utcTime_string;
    }

    public static String convertDatabaseDateTimeToDefault(Date a_objDate) throws Exception {

        String DATE_FORMAT_DATA_BASE_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
        String DATE_FORMAT_DATA_BASE_DATE_TIME_DEFAULT = "dd-MM-yyyy HH:mm:ss";

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE_TIME);
        String utcTime = sdf.format(a_objDate);

        SimpleDateFormat sdf_parse = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE_TIME);
        Date formatted_Date = sdf_parse.parse(utcTime);

        SimpleDateFormat sdf_string = new SimpleDateFormat(DATE_FORMAT_DATA_BASE_DATE_TIME_DEFAULT);
        String utcTime_string = sdf_string.format(formatted_Date);

        return utcTime_string;
    }




    public static void main(String[] arg) throws Exception {

        //System.out.println("testing ::::  "+DateFormatManager.getCurrentDate().toString());
        //System.out.println("testing ::::  "+DateFormatManager.getCurrentDateString());
        //System.out.println("testing ::::  "+DateFormatManager.getStringToDateSimpleFormat("10-08-15").toString());
        //String str_date = DateFormatManager.getUTCDateTime();
        //System.out.println("date in utc :::    "+str_date);
        //System.out.println("time in UTC :::  "+DateFormatManager.getUTCDateTime());
        //String str_date = "2015-08-11 13:21:10.0";
        //2015-08-11 13:21:10.0
        //String str_date = "2015-08-26 10:12:39";
        //String str_time = "10:37:06";
        //String dateTime = str_date + " "+str_time;
        //String date = getCurrentDateString();
        //DateFormatManager.parseBankPaymentDate(str_date, str_time);
        //DateFormatManager.parseUTCDateTimeToLocale();
        //DateFormatManager.getUTCDateTime();
        //String str_dateTime = DateFormatManager.parseUTCDateTimeToLocale(dateTime);
        //String str_dateTime = DateFormatManager.parseUTCDateTimeToLocale(str_date);
        //System.out.println(" date and time  "+str_dateTime);

        // System.out.println(" current date "+DateFormatManager.getUTCDateTime().toString());
        //  String string = "25-11-2015";
        // DateFormat format = new SimpleDateFormat(DATE_FORMAT_DD_MM_YY, Locale.ENGLISH);
        //Date date = format.parse(string);
        // System.out.println(date);
        // DateFormat outFormat = new SimpleDateFormat(DATE_FORMAT_DATA_BASE, Locale.ENGLISH);
        // String output = outFormat.format(date);
        //System.out.println(output); // 2013-12-04
/*        String dt = DateFormatManager.parseDefaultDateFormatToDataBaseDate("08-12-2015");
        //DateFormatManager.parseDefaultDateFormatToDataBaseDate(dt);
        System.out.println(dt);

        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        // 3 letter name form of the day
        System.out.println(new SimpleDateFormat("dd", Locale.ENGLISH).format(date.getTime()));*/

        getUTCDateStringPlusDays(3);
        // System.out.println(" current date is: "+DateFormatManager.parseFormDateToDataBaseDate(dt));
        //2015-08-26 10:12:39

    }

    public static String getStringToDateWithPoint(String a_datetimeForForm) {
        Date date = null;
        String utcTime_string = null;

        String[] formats = {
                "dd-MM-yyyy",   "dd-MM-yyyy HH:mm:ss"};

        if (a_datetimeForForm != null) {
            for (String parse : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(parse);
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    date = simpleDateFormat.parse(a_datetimeForForm);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        try {

            SimpleDateFormat sdf_string = new SimpleDateFormat(DATE_FORMAT_WITH_POINT);
            utcTime_string = sdf_string.format(date);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return utcTime_string;
    }

}
