package user.manager;

import user.beans.CustomerBean;
import user.beans.UserBean;

/**
 * Created by atticus on 18.06.17.
 */
public interface UserManager {

    UserBean checkUser(String user, String password);

    void newUser(CustomerBean customerBean, UserBean userBean);

    boolean isLoginFree(UserBean userBean);
}
