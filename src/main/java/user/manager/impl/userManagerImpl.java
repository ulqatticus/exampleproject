package user.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import user.beans.CustomerBean;
import user.beans.UserBean;
import user.dao.UserDao;
import user.database.entities.user.UserCustomerEntity;
import user.database.entities.user.UserEntity;
import user.manager.UserManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import user.utils.DateFormatManager;

/**
 * Created by atticus on 18.06.17.
 */
@Service
@Transactional
public class userManagerImpl implements UserManager {

    @Autowired
    UserDao userDao;

    public UserBean checkUser(String user, String password) {
        UserEntity userEntity = userDao.getUsersEntityByUserNameAndPassword(user, password);
        if(userEntity!=null){
            return UserEntityToUserBean(userEntity);
        }
        else return null;
    }

    public void newUser(CustomerBean customerBean, UserBean userBean) {
        UserEntity userEntity = parseUserBeanToUserEntity(userBean);
        UserCustomerEntity userCustomerEntity = parseCustomerBeanToCustomerEntity(customerBean);
  /*      String str_userId = generateUserId();
        userEntity.setId(Long.valueOf(str_userId));
        userCustomerEntity.setUserId(Long.valueOf(str_userId));*/
        userCustomerEntity.setCustomerId(userEntity);
        userEntity.setCustomerEntity(userCustomerEntity);

        userDao.saveOrUpdateUserEntity(userCustomerEntity);

        userDao.saveOrUpdateUserEntity(userEntity);




    }

    public boolean isLoginFree(UserBean userBean) {
        return userDao.checkOnDuplicateLogin(userBean.getLogin());
    }

    private UserCustomerEntity parseCustomerBeanToCustomerEntity(CustomerBean customerBean) {
        UserCustomerEntity userCustomerEntity = new UserCustomerEntity();
        userCustomerEntity.setFirstName(customerBean.getFirstName());
        userCustomerEntity.setSecondName(customerBean.getSecondName());
        userCustomerEntity.setCity(customerBean.getCity());
        userCustomerEntity.setGender(customerBean.getGender());
        userCustomerEntity.setEmail(customerBean.getEmail());
        return userCustomerEntity;
    }
/*
    private String generateUserId() {
        return "11";
    }*/

    private UserEntity parseUserBeanToUserEntity(UserBean customerBean) {
        UserEntity userEntity = new UserEntity();
        userEntity.setPassword(customerBean.getPassword());
        userEntity.setLogin(customerBean.getLogin());
        //userEntity.setEmail(customerBea);
        try {
            userEntity.setCreatedAt(DateFormatManager.getUTCDateTime());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return userEntity;
    }

    private UserBean UserEntityToUserBean(UserEntity userEntity) {
        UserBean userBean = new UserBean();
        userBean.setLogin(userEntity.getLogin());
        userBean.setPassword(userEntity.getPassword());
        return userBean;
    }
}
