package user.database.entities.user;



import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import user.database.entities.BaseEntity;
import user.database.enums.UserStatusEnum;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by atticus on 17.06.17.
 */
@Entity
@Table(name = "user")
public class UserEntity extends BaseEntity {
    private static final long serialVersionUID = 1L;
    @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "eMail")
    private String email;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdAt")
    private Date createdAt;

    @OneToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "customerId")
    private UserCustomerEntity customerEntity;

    public UserEntity() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public UserCustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(UserCustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }
}
