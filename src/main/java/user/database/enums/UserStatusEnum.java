package user.database.enums;

/**
 * Created by atticus on 17.06.17.
 */
public enum UserStatusEnum {
    NEW, ACTIVE, DISABLED, SUSPENDED
}
