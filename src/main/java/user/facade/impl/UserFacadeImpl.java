package user.facade.impl;

import user.beans.CustomerBean;
import user.beans.UserBean;
import user.facade.UserFacade;
import user.manager.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by atticus on 18.06.17.
 */
@Component
public class UserFacadeImpl implements UserFacade {

    @Autowired
    UserManager userManager;


    public UserBean checkUser(String user, String password) {
        UserBean userBean = userManager.checkUser(user,password);
        return userBean;
    }

    public void newUser(CustomerBean customerBean, UserBean userBean) {
        userManager.newUser(customerBean,userBean);
    }

    public boolean isLoginFree(UserBean userBean) {
        return  userManager.isLoginFree(userBean);
    }
}
